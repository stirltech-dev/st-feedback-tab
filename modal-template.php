
<a class="stfb-tab stfb-tab--<?= get_option( 'stfbt_location', 'right' ); ?>" style="background-color: <?= get_option( 'stfbt_bg_color', '#428bca' ); ?>;" href="#open-modal"><?= get_option( 'stfbt_text', 'Feedback' ); ?></a>
<div id="open-modal" class="stfbt-modal-window">
  <div>
    <a href="#modal-close" title="Close" class="stfbt-modal-close">Close</a>
    <h1><?= get_option( 'stfbt_text', 'Feedback' ); ?></h1>
    <div>
        <?= get_option( 'stfbt_content' ); ?>
        <?php if ( ( $form_id = get_option( 'stfbt_form_id' ) ) && function_exists( 'gravity_form' ) ) : ?>
            <?php gravity_form( 
                $form_id, 
                false, // display title
                false, // display description
                true, // display even if inactive
                null, // default field values
                true // ajax
            ); ?>
        <?php endif; ?>
    </div>
  </div>
</div>
<script>
    window.onload = function() {
        document.getElementById('open-modal').style.display = 'block';
    };
</script>