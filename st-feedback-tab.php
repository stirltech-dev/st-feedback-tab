<?php
/**
 * Plugin Name: ST Feedback Tab
 * Plugin URI: https://stboston.com
 * Description: Add a sitewide feedback tab to link to a page or form
 * Version: 0.1.1
 * Author: Brian Hanna, Stirling Technologies
 * Author URI: https://stboston.com
**/

class ST_Feedback_Tab {
    protected $settings_page_slug = 'st-feedback-tab';
    protected $settings_group_name = 'st_feedback_tab_settings';
    protected $settings_section_name = 'st_feedback_tab_settings_section';

    public function init() {
        $this->register_admin_hooks();
        $this->register_public_hooks();
    }

    public static function show_tab() {
        return get_option( 'stfbt_enabled' );
    }

    public function render_tab() {
        if ( !static::show_tab() ) {
            return;
        }
        wp_enqueue_style( 'stfbt-modal', plugin_dir_url( __FILE__ ) . 'modal.css' );
        include 'modal-template.php';
    }

    public function register_public_hooks() {
        add_action( 'wp_footer', [ $this, 'render_tab' ] );
    }

    public function register_admin_hooks() {
        add_action( 'admin_menu', [ $this, 'add_settings_page' ] );
        add_action( 'admin_init', [ $this, 'add_settings_sections' ] );
        add_action( 'admin_init', [ $this, 'register_settings' ] );
        add_action( 'admin_init', [ $this, 'add_settings_to_sections' ] );
    }

    public function add_settings_page() {
        add_options_page(
            'Feedback Tab Settings',
            'Feedback Tab',
            'manage_options',
            $this->settings_page_slug,
            [ $this, 'render_settings_page' ]
        );
    }

    public function render_settings_page() {
        if ( isset( $_GET['settings-updated'] ) ) {
            // add settings saved message with the class of "updated"
            add_settings_error( 'stfbt_messages', 'stfbt_message', __( 'Settings Saved' ), 'updated' );
        }    
        // show error/update messages
        settings_errors( 'stfbt_messages' );
        ?>
        <div class="wrap">
            <h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
            <form action="options.php" method="post">
                <?php settings_fields($this->settings_group_name); ?>
                <?php do_settings_sections($this->settings_page_slug); ?>
                <?php submit_button( 'Save Settings' ); ?>
            </form>
        </div>
        <?php
    }

    public function register_settings() {
        register_setting( $this->settings_group_name, 'stfbt_location', [ 'type' => 'string', 'default' => 'right' ] );
        register_setting( $this->settings_group_name, 'stfbt_text', [ 'type' => 'string', 'sanitize_callback' => [ $this, 'sanitize_with_icons' ], 'default' => 'Feedback' ] );
        register_setting( $this->settings_group_name, 'stfbt_page_id', [ 'type' => 'integer', 'default' => '' ] );
        register_setting( $this->settings_group_name, 'stfbt_content', [ 'type' => 'string', 'sanitize_callback' => 'sanitize_text_field', 'default' => '' ] );
        register_setting( $this->settings_group_name, 'stfbt_form_id', [ 'type' => 'integer', 'default' => '' ] );
        register_setting( $this->settings_group_name, 'stfbt_bg_color', [ 'type' => 'string', 'default' => '#428bca' ] );
        register_setting( $this->settings_group_name, 'stfbt_enabled', [ 'type' => 'integer', 'sanitize_callback' => 'sanitize_text_field', 'default' => 0 ] );
    }

    public function sanitize_with_icons( $string ) {
        return wp_kses($string, [
            'i' => [
                'class' => []
            ],
            'aria-hidden' => [],
            'strong' => [],
            'em' => []
        ]);
    }

    public function add_settings_sections() {
        add_settings_section( $this->settings_section_name, '', '', $this->settings_page_slug );
    }

    public function add_settings_to_sections() {
        add_settings_field( 'tab_location', 'Tab Location', [ $this, 'render_location_field' ], $this->settings_page_slug, $this->settings_section_name );
        add_settings_field( 'tab_text', 'Tab Text', [ $this, 'render_text_field' ], $this->settings_page_slug, $this->settings_section_name );
        //add_settings_field( 'tab_page_id', 'Link to Page', [ $this, 'render_page_id_field' ], $this->settings_page_slug, $this->settings_section_name );
        add_settings_field( 'tab_content', 'Popup Content', [ $this, 'render_tab_content_field' ], $this->settings_page_slug, $this->settings_section_name );
        add_settings_field( 'tab_form_id', 'Display Form', [ $this, 'render_form_id_field' ], $this->settings_page_slug, $this->settings_section_name );
        add_settings_field( 'tab_bg_color', 'Background Color', [ $this, 'render_bg_color_field' ], $this->settings_page_slug, $this->settings_section_name );
        add_settings_field( 'tab_enabled', 'Enable Feedback Tab', [ $this, 'render_enable_field' ], $this->settings_page_slug, $this->settings_section_name );
    }

    public function render_location_field() {
        $options = [
            'right' => 'Right',
            'left' => 'Left'
        ];

        $selected = get_option( 'stfbt_location' );
        
        echo '<select name="stfbt_location">';
        foreach ( $options as $value => $label ) {
            printf( '<option value="%s" %s>%s</option>', $value, selected( $selected, $value, false ), $label );
        }
        echo '</select>';
    }

    public function render_text_field() {
        echo '<input type="text" name="stfbt_text" value="' . esc_attr( get_option( 'stfbt_text' ) ) . '" placeholder="Feedback" />';
    }

    public function render_page_id_field() {
        wp_dropdown_pages([
            'selected' => get_option( 'stfbt_page_id'),
            'name' => 'stfbt_page_id'
        ]);
    }

    public function render_bg_color_field() {
        echo '<input type="text" name="stfbt_bg_color" value="' . esc_attr( get_option( 'stfbt_bg_color' ) ) . '" placeholder="#428bca" />';
    }
    
    public function render_enable_field() {
        echo '<label><input type="checkbox" name="stfbt_enabled" value="1" ' . checked( get_option( 'stfbt_enabled' ), 1, false ) .'/> Enable</label>';
    }

    public function render_tab_content_field() {
        echo '<textarea name="stfbt_content" style="width:600px;max-width:100%;" rows="10">' . esc_textarea( get_option( 'stfbt_content' ) ) . '</textarea>';
    }

    public function render_form_id_field() {
        if ( !class_exists( 'GFAPI' ) ) {
            echo 'You must enable Gravity Forms to use this functionality.';
            return;
        }
        $forms = GFAPI::get_forms();
        $selected = get_option( 'stfbt_form_id' );
        echo '<select name="stfbt_form_id">';
        echo '<option value=""></option>';
        foreach ( $forms as $form ) {
            printf( '<option value="%s" %s>%s</option>', $form['id'], selected( $selected, $form['id'] ), $form['title'] );
        }
        echo '</select>';
    }
}

function _run_stfbt_plugin() {
    $fbt = new ST_Feedback_Tab;
    $fbt->init();
}

_run_stfbt_plugin();